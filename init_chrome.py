from selenium import webdriver
import os


def returnos():
    return os.name
def start():
    if returnos()=="posix":
        driver = webdriver.Chrome(executable_path='drivers/linux/chromedriver')
    elif returnos()=="nt":
        driver = webdriver.Chrome(executable_path='drivers/windows/chromedriver.exe')
    driver.maximize_window()
    return driver

def start_minize():
    driver = webdriver.Chrome(executable_path='drivers/linux/chromedriver')
    driver.minimize_window()
    return driver


if __name__ == '__main__':
    start()
    print(returnos())
