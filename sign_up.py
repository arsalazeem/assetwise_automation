from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
import time
import random
import pdb
import init_chrome
import os
from pynput.keyboard import Key, Controller
import add_site



keyboard = Controller()
payload={
    "f_name":"arsal",
    "l_name":"azeem",
    "email":"sweet"+str(random.randint(1,1000))+"@vizteck.com",
    "password":"12345678",
    "c_name":"vizteckians",
    "country_name":"Pakistan",
    "adress":"this is the adress",
    "apt_suite":"nothing",
    "state":"Federal",
    "city":"islamabad",
    "postal_code":4600,
    "image_path":os.getcwd()+"/a.png"
}

def writekeys(driver,path,text):
    driver.find_element(By.XPATH,path).send_keys(text)
    return driver
def makeclick(driver,path_name,method="click"):
    if method=="click":
        driver.find_element(By.XPATH, path_name).click()
        return driver
    if method=="name":
        driver.find_element(By.NAME,path_name).click()
        return driver
def write_by_class(driver,cls,text="none"):
    driver.find_element(By.CSS_SELECTOR,cls).click()
    return driver

def writekey_withname(driver,name,text):
    driver.find_element(By.NAME,name).send_keys(text)
    return driver

def drop_down_select(driver,name,text):
    select = Select(driver.find_element(By.NAME,name))
    select.select_by_visible_text(text)
    return driver
def write_by_name(driver,name,text):
    visibility_check=driver.find_element(By.NAME,name)
    print(name)
    print("visibility of the element is:"+str(visibility_check.is_displayed()))
    if visibility_check.is_displayed()==True:
        driver.find_element(By.NAME, name).send_keys(text)
    else:
        print("Visibilty of this element is:"+str(visibility_check.is_displayed())+" "+" So we cant click.")
    return driver

def click_btn_by_text(btn_text):
    driver.find_element_by_xpath('//button[contains(text(),'+btn_text+')]').click()

def click_continue(driver):
    time.sleep(1)
    # WebDriverWait(driver, 10).until(
    #     EC.presence_of_element_located((By.XPATH, "myDynamicElement"))
    # )
    driver.find_element_by_xpath('//button[contains(text(), "Continue")]').click()
    return driver
def tab_method():
    time.sleep(1)
    keyboard.press(Keys.TAB)
    # keyboard.release(Keys.TAB)



driver=init_chrome.start()
driver.get("http://54.186.118.166:3600/auth/signup-1")
driver.implicitly_wait(30)
driver=write_by_name(driver,"first_name",payload["f_name"])
driver=write_by_name(driver,"last_name",payload["l_name"])
driver=write_by_name(driver,"email",payload["email"])
driver=write_by_name(driver,"password",payload["password"])
driver=write_by_name(driver,"retype_password",payload["password"])
driver=write_by_name(driver,"companyName",payload["c_name"])
driver=makeclick(driver,"terms","name")
driver.find_element(By.TAG_NAME,"button").click()
driver=drop_down_select(driver,"country","Pakistan")
driver=write_by_name(driver,"companyName",payload["c_name"])
driver=write_by_name(driver,"address",payload["adress"])
driver=write_by_name(driver,"apt_suite",payload["apt_suite"])
driver=write_by_name(driver,"city",payload["city"])
driver=write_by_name(driver,"state",payload["state"])
driver=write_by_name(driver,"zip",payload["postal_code"])
driver=drop_down_select(driver,"timezone","(GMT -10:00) Hawaii")
driver=drop_down_select(driver,"currency","Pakistani Rupee(PKR)")
driver=drop_down_select(driver,"date_format","MM/dd/yyyy")
driver=drop_down_select(driver,"finencial_yr_begins_on_month","June")
driver=drop_down_select(driver,"finencial_yr_begins_on_date","4")
driver=write_by_name(driver,"zip",payload["postal_code"])
driver.find_element_by_xpath('//*[@id="root"]/div/div/div[2]/div/div[2]/div/div/div[2]/div/div/div/div[2]/div/button').click()
time.sleep(1)
driver=write_by_name(driver,"company_logo",payload["image_path"])
time.sleep(1)
driver.find_element_by_xpath('//button[contains(text(), "Done")]').click()
driver=click_continue(driver)
time.sleep(1)
#started adding sites add here
add_site.add_site(driver)
#finished adding sites
pdb.set_trace()
driver.quit()
print("Executed completely")