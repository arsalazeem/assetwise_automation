from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
import time
import random
import pdb
import init_chrome
import os
from pynput.keyboard import Key, Controller

keyboard = Controller()


payload={
    "f_name":"arsal",
    "l_name":"azeem",
    "email":"sweet"+str(random.randint(1,1000))+"@vizteck.com",
    "password":"12345678",
    "c_name":"vizteckians",
    "country_name":"Pakistan",
    "adress":"this is the adress",
    "apt_suite":"nothing",
    "state":"Federal",
    "city":"islamabad",
    "postal_code":4600,
    "image_path":os.getcwd()+"/a.png"
}



def write_by_name(driver,name,text):
    visibility_check=driver.find_element(By.NAME,name)
    print(name)
    print("visibility of the element is:"+str(visibility_check.is_displayed()))
    if visibility_check.is_displayed()==True:
        driver.find_element(By.NAME, name).send_keys(text)
    else:
        print("Visibilty of this element is:"+str(visibility_check.is_displayed())+" "+" So we cant click.")
    return driver

def tab_method():
    time.sleep(1)
    keyboard.press(Keys.TAB)
    # keyboard.release(Keys.TAB)


def click_continue(driver):
    time.sleep(1)
    # WebDriverWait(driver, 10).until(
    #     EC.presence_of_element_located((By.XPATH, "myDynamicElement"))
    # )
    driver.find_element_by_xpath('//button[contains(text(), "Continue")]').click()
    return driver


def add_site(driver):
    driver = write_by_name(driver, "siteName", payload["city"])
    driver.find_element(By.XPATH,
                        '//*[@id="root"]/div/div/div[2]/div/div[2]/div/div/div[2]/div/div/div[2]/div[2]/form/div[2]/div/div').click()
    tab_method()
    keyboard.type('This is our main site')
    driver = write_by_name(driver, "address", payload["adress"])
    driver = write_by_name(driver, "apt_suite", payload["apt_suite"])
    driver = write_by_name(driver, "city", payload["city"])
    driver = write_by_name(driver, "state", payload["state"])
    driver = write_by_name(driver, "zip", payload["postal_code"])
    driver = write_by_name(driver, "country", payload["country_name"])
    driver = write_by_name(driver, "description", payload["city"])
    driver.find_element_by_xpath('//button[contains(text(), "Add Site")]').click()
    click_continue(driver)



